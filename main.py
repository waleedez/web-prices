from typing import Text
from bs4 import BeautifulSoup
import requests
from flask import Flask

app = Flask(__name__)
@app.route('/')
def index():
  # URL of the HTML page
  url = 'https://market.isagha.com/prices'
  
  # Make a request to the URL
  response = requests.get(url)
  
  # Check if the request was successful (status code 200)
  if response.status_code == 200:
      # Parse the HTML content using BeautifulSoup
      soup = BeautifulSoup(response.text, 'html.parser')
  
      return soup.find_all('span')[1].parent.parent.find_all('div')[6].get_text()
  else:
      return "0"
    